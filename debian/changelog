osmosis (0.49.2-3) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.7.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 28 Jul 2024 19:48:58 +0200

osmosis (0.49.2-2) unstable; urgency=medium

  * Add autopkgtest to verify PBF to XML conversion.
  * Fix plexus configuration.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 08 Dec 2023 17:22:12 +0100

osmosis (0.49.2-1) unstable; urgency=medium

  * New upstream release.
  * Update Maven packaging for JPF dependency.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 03 Dec 2023 09:31:36 +0100

osmosis (0.49.1-1) unstable; urgency=medium

  * Drop obsolete 04-osmosis-version.patch.
  * Various ShellCheck fixed for osmosis launcher.
  * Drop obsolete default-jdk-doc build dependency.
  * Strip unusable maven:CompileDepends from substvars.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 11 Nov 2023 07:21:48 +0100

osmosis (0.49.1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add Rules-Requires-Root to control file.
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Remove generated files in clean target.
  * Enable Salsa CI.
  * Switch to dh-sequence-*.
  * Update install paths.
  * Refresh patches.
  * Drop obsolete (build) dependencies.
  * Switch build system from gradle to maven.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 10 Nov 2023 14:53:52 +0100

osmosis (0.48.3-2) unstable; urgency=medium

  * Bump watch file version to 4.
  * Update lintian overrides.
  * Bump Standards-Version to 4.6.1, no changes.
  * Update upstream metadata.
  * Update watch file for GitHub URL changes.
  * Bump debhelper compat to 12, no changes.
  * Add upstream patch to fix build with postgis-java 2021.1.0.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 12 Aug 2022 11:47:26 +0200

osmosis (0.48.3-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 09 Sep 2020 05:49:51 +0200

osmosis (0.48.2-1) unstable; urgency=medium

  * New upstream release.
  * Drop protobuf.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 14 Jul 2020 20:16:04 +0200

osmosis (0.48.1-2) unstable; urgency=medium

  * Rebuild with protobuf 3.12.2.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 08 Jul 2020 06:11:57 +0200

osmosis (0.48.1-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 30 Jun 2020 19:09:26 +0200

osmosis (0.48.0-1) unstable; urgency=medium

  * New upstream release.
  * Add libcommons-{csv,io}-java to (build) dependencies.
  * Refresh patches.
  * Add patch to not require shadow library, not available in buster.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 05 May 2020 19:37:14 +0200

osmosis (0.47.4-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 08 Apr 2020 05:59:22 +0200

osmosis (0.47.3-1) unstable; urgency=medium

  * New upstream release.
  * Drop protobuf.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 24 Mar 2020 17:33:41 +0100

osmosis (0.47.2-2) unstable; urgency=medium

  * Add patch to fix FTBFS with protobuf 3.11.4.
    (closes: #944928)
  * Require at least libprotobuf-java 3.11.4.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 22 Mar 2020 10:10:37 +0100

osmosis (0.47.2-1) unstable; urgency=medium

  * New upstream release.
  * Don't use external xerces.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 11 Mar 2020 06:03:18 +0100

osmosis (0.47.1-1) unstable; urgency=medium

  * New upstream release.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.5.0, no changes.
  * Drop Name field from upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 09 Mar 2020 05:50:37 +0100

osmosis (0.47-4) unstable; urgency=medium

  * Bump Standards-Version to 4.3.0, no changes.
  * Apply patch by Emmanuel Bourg to fix FTBFS with updates plexus-classworlds.
    (closes: #923564)

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 02 Mar 2019 08:12:06 +0100

osmosis (0.47-3) unstable; urgency=medium

  * Apply patches by Markus Koschany to use libmariadb-java.
    (closes: #913307)

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 09 Nov 2018 16:14:34 +0100

osmosis (0.47-2) unstable; urgency=medium

  * Drop patches for protobuf 3.0.0.
  * Require at least libprotobuf-java 3.6.1.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 13 Oct 2018 15:48:37 +0200

osmosis (0.47-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.2.1, no changes.
  * Drop example-shell-script-fails-syntax-check.patch, applied upstream.
  * Add patch to revert protobuf 3.6.1 changes.
  * Add patch to regenerate protobuf files with protobuf 3.0.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 29 Sep 2018 10:55:11 +0200

osmosis (0.46-7) unstable; urgency=medium

  * Update watch file to use releases instead of tags.
  * Update watch file to limit matches to archive path.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 18 Aug 2018 18:08:26 +0200

osmosis (0.46-6) unstable; urgency=medium

  * Bump Standards-Version to 4.2.0, no changes.
  * Add patch to disable building osmosis-replication-http module,
    requires Netty 3.x which is being removed from Debian.
    (closes: #905858)
  * Add NEWS file to document HTTP replication removal.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 11 Aug 2018 11:14:02 +0200

osmosis (0.46-5) unstable; urgency=medium

  * Bump Standards-Version to 4.1.5, no changes.
  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 11:18:40 +0200

osmosis (0.46-4) unstable; urgency=medium

  * Update Vcs-* URLs for Salsa.
  * Drop override for vcs-deprecated-in-debian-infrastructure.
  * Bump Standards-Version to 4.1.4, no changes.
  * Strip trailing whitespace from control & rules files.
  * Build depend on default-jdk & default-jdk-doc, fixes FTBFS:
    javadoc: error - Error fetching URL: file:/usr/share/doc/default-jdk/api/
  * Add patch to fix example-shell-script-fails-syntax-check issue.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 20 Jun 2018 21:21:12 +0200

osmosis (0.46-3) unstable; urgency=medium

  * Update copyright-format URL to use HTTPS.
  * Bump Standards-Version to 4.1.3, no changes.
  * Add lintian override for vcs-deprecated-in-debian-infrastructure.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 29 Mar 2018 14:59:30 +0200

osmosis (0.46-2) unstable; urgency=medium

  * Fix README.md installation.
  * Bump Standards-Version to 4.1.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 02 Oct 2017 08:34:57 +0200

osmosis (0.46-1) unstable; urgency=medium

  * New upstream release.
  * Drop protobuf-3.0.0.patch, applied upstream.
    Refresh remaining patches.
  * Bump Standards-Version to 4.1.0, no changes.
  * Update alternative JRE dependency to require Java 8.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 26 Sep 2017 16:52:36 +0200

osmosis (0.45-5) unstable; urgency=medium

  * Apply patch by Emmanuel Bourg to migrate to libplexus-classworlds2-java.
    (closes: #873886)
  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 01 Sep 2017 02:04:38 +0200

osmosis (0.45-4) unstable; urgency=medium

  * Update maven.rules for new postgresql-jdbc.
    (closes: #850993)

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 11 Jan 2017 22:35:55 +0100

osmosis (0.45-3) unstable; urgency=medium

  * Switch to headless variant for default-jdk dependency.
  * Add patch to support protobuf 3.0.0.
    (closes: #835776)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 28 Aug 2016 18:06:06 +0200

osmosis (0.45-2) unstable; urgency=medium

  * Update maven.rules for spring-jdbc.
    (closes: #832869)

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 29 Jul 2016 12:39:14 +0200

osmosis (0.45-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Git URL to use HTTPS.
  * Update watch file to handle more tag conventions.
  * Bump Standards-Version to 3.9.8, no changes.
  * Drop 06-java-7.patch, applied upstream.
  * Add maven rule for com.fasterxml.woodstox woodstox-core.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 27 May 2016 17:32:11 +0200

osmosis (0.44.1-4) unstable; urgency=medium

  * Use postgis-jdbc.jar instead of old postgis.jar symlink in plexus.conf.
    Symlink not provided by libpostgis-java (>= 2.2.0).
    (closes: #808255)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 17 Dec 2015 22:34:44 +0100

osmosis (0.44.1-3) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Update Vcs-Browser and various openstreemap.org URLs to use HTTPS.

  [ Emmanuel Bourg ]
  * Removed the build dependency on checkstyle
  * Build with gradle-debian-helper

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 14 Dec 2015 23:31:54 +0100

osmosis (0.44.1-2) unstable; urgency=medium

  * Team upload.
  * Depend on libnetty-3.9-java instead of libnetty-java

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 28 Jul 2015 16:00:57 +0200

osmosis (0.44.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop patches applied upstream, refresh remaining patches.
  * Update alternative JRE dependency for Java 7 to match sourceCompatibility.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 04 Jun 2015 19:10:52 +0200

osmosis (0.43.1-6) unstable; urgency=medium

  * Fix malformed opts=... in watchfile.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 30 May 2015 14:43:15 +0200

osmosis (0.43.1-5) unstable; urgency=medium

  * Don't compress SQL examples to allow piping into psql with just cat.
  * Add patch from upstream to fix java.lang.ClassCastException for
    java.util.HashMap to org.openstreetmap.osmosis.hstore.PGHStore.
    (closes: #785257)
  * Add patches from upstream master branch, changes include:
    - db-server
    - way-nodes fixes
    - replication URL updates to match current planet.osm.org layout

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 14 May 2015 16:21:36 +0200

osmosis (0.43.1-4) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Apr 2015 21:05:47 +0200

osmosis (0.43.1-4~exp1) experimental; urgency=medium

  * Update 02-fix_plexus.patch to also load Xerces to fix data corruption.
    (closes: #779807)
  * Update my email to @debian.org address.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 Mar 2015 09:45:06 +0100

osmosis (0.43.1-3~exp1) experimental; urgency=medium

  * Update watch file to use github tags instead of the deprecated
    githubredir service.
  * Bump Standards-Version to 3.9.6, no changes.
  * Add upstream metadata.
  * Add patch to fix negative exit status in replicate_osm_file.sh.
    (closes: #772355)
  * Update Vcs-Browser URL to use cgit instead of gitweb.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sat, 10 Jan 2015 13:56:29 +0100

osmosis (0.43.1-2) unstable; urgency=medium

  * Add patch to update generated source for protobuf 2.6.0.
    (closes: #761562)
  * Add patch to increase source compatibility from 1.6 to 1.7.
  * Add patches from upstream master branch.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sat, 27 Sep 2014 21:43:24 +0200

osmosis (0.43.1-1) unstable; urgency=medium

  [ Giovanni Mascellani ]
  * Remove myself from the uploaders list, since I have no time
    anymore to look after this package.

  [ Andreas Tille ]
  * moved to pkg-grass Git repository

  [ Bas Couwenberg ]
  * New upstream release.
    (closes: #710039, #700311)
  * Change Maintainer to Debian GIS Project.
  * Add myself to Uploaders.
  * Restructure control file with cme, changes: Vcs-* URLs.
  * Add gbp.conf to use pristine-tar by default.
  * Drop get-orig-source target, repacking no longer required.
  * Remove dependencies on ant, gradle now used for build.
    (closes: 703635)
  * Enable parallel builds.
  * Bump Debhelper compatibility to 9.
  * Use dh with maven-repo-helper.
  * Add build dependency on libwoodstox-java.
  * Add patch to use packaged libraries during build.
    (closes: #713278)
  * Remove generated files and directories on clean.
  * Bump Standards-Version to 3.9.5, changes: Vcs-* fields.
  * Add patch to get version from changelog instead of git.
  * Add patches from upstream master branch.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sun, 08 Jun 2014 09:54:55 +0200

osmosis (0.40.1+ds1-7) unstable; urgency=low

  * Added missing dependency on libxz-java (Closes: #680820)

 -- David Paleino <dapal@debian.org>  Tue, 10 Jul 2012 09:10:35 +0200

osmosis (0.40.1+ds1-6) unstable; urgency=low

  * Use a dedicate ivy cache, to don't interfere with user cache.
  * Fix some mistakes in dependency resolution, that led to FTBFS
    (closes: #674314).

 -- Giovanni Mascellani <gio@debian.org>  Fri, 15 Jun 2012 12:24:22 +0200

osmosis (0.40.1+ds1-5) unstable; urgency=low

  * Bump dependency on libcommons-compress-java (Closes: #664597)
  * Bump dependency on checkstyle (Closes: #664602)
  * Updated debian/copyright

 -- David Paleino <dapal@debian.org>  Sat, 14 Apr 2012 19:19:28 +0200

osmosis (0.40.1+ds1-4) unstable; urgency=low

  * Switch dependency from libpg-java to libpostgresql-jdbc-java
    (Closes: #659399)
  * Drop junit4 workaround: now Debian has 4.10
  * Bump Standards-Version to 3.9.3, no changes needed
  * Updated debian/copyright

 -- David Paleino <dapal@debian.org>  Thu, 08 Mar 2012 08:23:40 +0100

osmosis (0.40.1+ds1-3) unstable; urgency=low

  * Add missing Osmosis JARs to Plexus config (Closes: #656432)

 -- David Paleino <dapal@debian.org>  Fri, 20 Jan 2012 12:24:34 +0100

osmosis (0.40.1+ds1-2) unstable; urgency=low

  * Removing references to libwoodstox-java, which is not necessary to
    build osmosis.

 -- Giovanni Mascellani <gio@debian.org>  Wed, 11 Jan 2012 16:53:25 +0100

osmosis (0.40.1+ds1-1) unstable; urgency=low

  * New upstream version
  * Fix debian/watch and debian/rules' get-orig-source to point to github
  * Refresh patches to apply to the new source
    - added 04-fix_dependencies_versions.patch and
      05-workaround_missing_junit4.10.patch
  * Fix links to protobuf's jar (Closes: #650617, #652754)
  * Add dependencies to libxerces2-java

 -- David Paleino <dapal@debian.org>  Fri, 30 Dec 2011 00:29:51 +0100

osmosis (0.39+ds1-6) unstable; urgency=low

  * Fix plexus config file to correctly point to jars (Closes: #647452)

 -- David Paleino <dapal@debian.org>  Wed, 02 Nov 2011 21:54:01 +0100

osmosis (0.39+ds1-5) unstable; urgency=low

  * Add dependency to libcommons-pool-java (Closes: #644712)
  * Provide a log4j config file, thanks to Alberto Fernández (Closes: #579206)
  * Osmosis configuration file is now set to /etc/osmosis/osmosis.conf
    (Closes: #644841)

 -- David Paleino <dapal@debian.org>  Sun, 09 Oct 2011 20:12:58 +0200

osmosis (0.39+ds1-4) unstable; urgency=low

  * 00-fix_build_system.patch updated: don't hardcode version number
    in jar filenames.
  * 01-fix_launcher.patch fixed: osmosis.jar doesn't exist anymore.
  * 02-fix_plexus.patch: use ${app.home} instead of hardcoding osmosis
    path.

 -- David Paleino <dapal@debian.org>  Sat, 01 Oct 2011 22:21:58 +0200

osmosis (0.39+ds1-3) unstable; urgency=low

  * Fix plexus config file not to load duplicate jars, by hardcoding
    osmosis' version number in it.

 -- David Paleino <dapal@debian.org>  Thu, 29 Sep 2011 11:44:16 +0200

osmosis (0.39+ds1-2) unstable; urgency=low

  * Complete switch from Spring Framework 2.5 to 3.0 (Closes: #638539)
  * Add unversioned .jar symlinks to /usr/share/osmosis/

 -- David Paleino <dapal@debian.org>  Wed, 28 Sep 2011 20:33:50 +0200

osmosis (0.39+ds1-1) unstable; urgency=low

  [ David Paleino ]
  * New upstream release (closes: #605698)
  * Fix get-orig-source target in debian/rules
  * Use debhelper compatibility 8
  * Bump Standards-Version to 3.9.2, no changes needed
  * Fix debian/rules to support multi-archified java
  * Package moved from pkg-grass to pkg-osm
  * Package switched from cdbs to dh7

  [ Giovanni Mascellani ]
  * debian/watch: updated
  * debian/patches:
    - 00-fix_build.patch: removed, build system has completely changed;
    - 01-fix_launcher.patch, 02-fix_plexus.patch: files moved, conflicts
      resolved.
  * Depend on headless runtime environment and use default-jdk at
    building time (closes: #613135).

 -- David Paleino <dapal@debian.org>  Wed, 28 Sep 2011 19:03:10 +0200

osmosis (0.34+ds1-1) unstable; urgency=low

  [ David Paleino ]
  * New upstream version
  * debian/control:
    - updated my e-mail address
    - DMUA removed
    - added missing Build-Dependencies: libcommons-compress-java,
      libcommons-codec-java
    - added pkg-osm to Uploaders
    - Standards-Version bumped to 3.8.4, no changes needed
    - rewrapped for easier diff'ing
    - add alternative openjdk-6-jre dependency to java6-runtime
    - add dependency on libplexus-classworlds-java, libcommons-codec-java
      and libcommons-compress-java
  * debian/rules:
    - updated get-orig-source target to strip unwanted *.jar files
  * debian/copyright updated
  * debian/source/format added, using 3.0 (quilt)
  * debian/patches/:
    - 00-fix_build.patch added, fixes FTBFS because of missing file
      in upstream tarball
    - 01-fix_launcher.patch added, correctly point to the right paths
    - 02-fix_plexus.patch added, fix Plexus configuration with the
      right paths
  * debian/osmosis.1 statically generated, so to remove unneeded
    Build-Dependencies
  * debian/watch: use dversionmangle
  * debian/install: install plexus.conf in /etc/osmosis/

  [ Giovanni Mascellani ]
  * debian/*: updated my email address
  * debian/watch: updated to new upstream distribution location

 -- David Paleino <dapal@debian.org>  Thu, 01 Apr 2010 01:13:13 +0200

osmosis (0.31.2-2) unstable; urgency=low

  [ Giovanni Mascellani ]
  * Fixed some path mistakes and missing files in output JAR
    (closes: #550151, #550149)
  * Vcs-* fields updated

 -- David Paleino <d.paleino@gmail.com>  Fri, 09 Oct 2009 00:53:42 +0200

osmosis (0.31.2-1) unstable; urgency=low

  [ Giovanni Mascellani ]
  * Initial release (Closes: #497457).

  [ Francesco Paolo Lovergine ]
  * Added docbook-xml among build-dep.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 05 Oct 2009 22:25:18 +0200
